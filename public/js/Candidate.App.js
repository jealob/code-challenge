const app = angular.module("Candidate.App", []);

app.component("itmRoot", {
    controller: class {
        constructor() {
            this.candidates = [{ name: "Puppies", votes: 10 }, { name: "Kittens", votes: 12 }, { name: "Gerbils", votes: 7 }];
        }

        onVote(candidate) {
            // Votes for candidate          
            candidate.votes++;
        }

        onAddCandidate(candidate) {
            // Adds a new Object to the array 
            let newArray = [];
            // Ensure entry is not empty
            if (candidate.name) {
                // Ensure there is no duplicates
                angular.forEach(this.candidates, function (element) {
                    if (candidate.name.toLowerCase() === element.name.toLowerCase())
                        this.push(element);
                }, newArray);
                // if new element is empty then there is no duplicate
                if (newArray.length === 0)
                    this.candidates.push(
                        {
                            name: candidate.name,
                            votes: 0
                        });
                else
                    alert("Candidate already in list!");
            }
            else {
                alert("Must enter a name for candidate!");
            }
        }

        onRemoveCandidate(candidate) {
            const filterArray = this.candidates.filter(element => element.name !== candidate.name);
            this.candidates = filterArray;
        }

        getTotalVote() {
            const total = this.candidates.reduce((sum, candidate) => sum + candidate.votes, 0);
            // Ensure that there is no division by zero
            if (total < 1)
                return 1;
            else
                return total;
        }

        sortedCandidates() {
            return this.candidates.sort((a, b) => (a.votes < b.votes) ? 1 : ((b.votes < a.votes) ? -1 : 0));
        }
    },
    template: `
    <section id = "main-container">
        <div class="row welcome>
            <div class="text-center my-2 mx-auto">
                <h1>Which candidate brings the most joy?</h1>
            </div>
        </div>
        <div class="row rounded mb-3">
            <div class=" col-lg-5 col-md-12 col-sm-12 col-xs-12 py-3 my-2 ml-5 mx-auto text-center rounded results">
                <itm-results 
                    candidates="$ctrl.sortedCandidates()"
                    get-total-vote = "$ctrl.getTotalVote()">
                </itm-results>
            </div>
            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 py-3 my-2 mx-auto text-center rounded votes">
                <itm-vote 
                    candidates="$ctrl.candidates"
                    on-vote="$ctrl.onVote($candidate)">
                </itm-vote>
            </div>
        </div>

        <div class="rounded mb-3 management">
            <itm-management 
                candidates="$ctrl.candidates"
                on-add="$ctrl.onAddCandidate($candidate)"
                on-remove="$ctrl.onRemoveCandidate($candidate)">
            </itm-management>
        </div>
    </section> 
    `
});

app.component("itmManagement", {
    bindings: {
        candidates: "<",
        onAdd: "&",
        onRemove: "&"
    },
    controller: class {
        constructor() {
            this.newCandidate = {
                name: ""
            };
        }

        submitCandidate(candidate) {
            this.onAdd({ $candidate: candidate });
        }

        removeCandidate(candidate) {
            this.onRemove({ $candidate: candidate });
        }
    },
    template: `
        <h2 class = "text-center">Manage Candidates</h2>
        <div class="row">
            <div class=" col-lg-5 col-md-12 col-sm-12 col-xs-12 py-3 mx-auto text-center rounded">
                <h3>Add New Candidate</h3>
                <form ng-submit="$ctrl.submitCandidate($ctrl.newCandidate)" novalidate>

                    <label>Candidate Name</label>
                    <input type="text" ng-model="$ctrl.newCandidate.name" required>

                    <button type="submit" class= "btn btn-primary rounded mx-1">Add</button>
                </form>
            </div>
        
            <div class=" col-lg-5 col-md-12 col-sm-12 col-xs-12 py-3 mx-auto text-center rounded">
                <h3>Remove Candidate</h3>    
                <ul>
                    <li class="text-center my-1 mx-1" ng-repeat="candidate in $ctrl.candidates">
                        <span id="list-container" ng-bind="candidate.name"></span>
                        <button type="button" class="btn btn-danger" ng-click="$ctrl.removeCandidate(candidate)">X</button>
                    </li>
                </ul>
            </div>
        </div>
    `
});

app.component("itmVote", {
    bindings: {
        candidates: "<",
        onVote: "&"
    },
    controller: class { },
    template: `
        <h2>Cast your vote!</h2>

        <button type="button" class= "rounded mx-1" 
            ng-repeat="candidate in $ctrl.candidates"
            ng-click="$ctrl.onVote({ $candidate: candidate })">
            <span ng-bind="candidate.name"></span>
        </button>
    `
});

app.component("itmResults", {
    bindings: {
        candidates: "<",
        getTotalVote: "&"
    },
    controller: class { },
    template: `
        <h2>Live Results</h2>
        <ul>
            <li ng-repeat="candidate in $ctrl.candidates">
                <span ng-bind="candidate.name"></span>
                <strong ng-bind="candidate.votes"></strong>
                <strong>Percentage: <span ng-bind="(candidate.votes/$ctrl.getTotalVote() * 100).toFixed(1)"></span>%</strong>
            </li>
        </ul>
    `
});
